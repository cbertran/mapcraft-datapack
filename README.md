![logo](https://gitlab.com/cbertran/mapcraft-datapack/-/raw/main/icon.png "Logo")

## Mapcraft-datapack is the datapack allowing Mapcraft software to interact with Minecraft Java Edition

## Setup
### <span>&#x26a0;</span> If you wish to have access to all the possibilities offered by Mapcraft, download [the application](https://gitlab.com/cbertran/mapcraft)

Nevertheless you can use it as is, but you will only have access to the *editing* and *drawing* tools.
1. Download the latest stable version [here](https://gitlab.com/cbertran/mapcraft-datapack/-/releases), and install it in the ``datapacks`` folder of your world.
2. Download the latest stable version of Mapcraft base resource pack [here](https://gitlab.com/cbertran/mapcraft-resourcepack/-/releases), and install it in the ``resourcepacks`` folder of your directory game.

## Developer(s)
- Clément Bertrand [(@cbertran)](https://gitlab.com/cbertran)

## License
    Mapcraft Copyright (C) 2020 - 2022  Clément Bertrand
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
